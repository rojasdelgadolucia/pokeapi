package com.example.dogapi.modelo

import com.example.dogapi.Dog
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
interface DogApi {
    @Headers("Accept: application/json")
    // Método para obtener todos los pokemon
    @GET("breeds/image/random")
    fun getDog(): Call<Dog>
}