package com.example.dogapi

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Modifier
import com.example.dogapi.modelo.DogApi
import com.example.dogapi.ui.theme.PokeapiTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
class MainActivity : ComponentActivity() {
    private lateinit var retrofit : Retrofit
    private var texto: String = "Pruebas"
    override fun onCreate( savedInstanceState: Bundle?) {
        retrofit = Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/breeds/image/random")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        texto = obtenerDatos(retrofit)
        super.onCreate(savedInstanceState)
        setContent {
            PokeapiTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Text(text = "IMAGEN DE PERRO")
                    Text(text = texto)

                }
            }
        }
    }
    private fun obtenerDatos(retrofit : Retrofit) : String{
        var texto = "";
        CoroutineScope(Dispatchers.IO).launch {
            val call =
                retrofit.create(DogApi::class.java).getDog().execute()
            val dogs = call.body()
            if(call.isSuccessful){
                texto = dogs?.getMessage().toString()
            }else{
                texto = "Ha habido un error"
            }
        }
        Thread.sleep(1000)
        return texto;
    }
}
class Dog {
    private lateinit var message: String;
    fun getMessage() : String{
        return this.message
    }
    fun setMessage(message : String){
        this.message = message
    }
}